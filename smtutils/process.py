
import subprocess
import distutils.spawn
import ConfigParser
from typing import AnyStr

def get_msat_path(name):
    # type: (str) -> str
    "old, replace with path finding code"
    path = distutils.spawn.find_executable(name)
    if not path:
        conf = ConfigParser.ConfigParser.read("paths.txt")
        path = conf[name]
    return path

class Solver(object):
    "Class representing SMT solver processes"
    def __init__(self, path):
        # type: (str) -> None
        "Initialize process: path is the executable path"
        self.binpath = path
        self.process = subprocess.Popen(self.binpath, stdin=subprocess.PIPE, stdout=subprocess.PIPE)

    def load_formula(self, formula):
        # type: (AnyStr) -> None
        " xxx deadlock do not use"
        self.process.stdin.write(str(formula))
        self.process.stdin.write("\n")
        self.process.stdin.flush()

    def get_result(self):
        # type: () -> str
        " xxx deadlock do not use"
        return self.process.stdout.read()

    def run_formula(self, formula):
        # type: (AnyStr) -> str
        "Run a formula with the solver, not interactive"
        return self.process.communicate(str(formula))[0]