import sexpdata
from fractions import Fraction

def parseRational(v):
    if hasattr(v, '__len__'):
        if v[0].value() == "-":
            return -parseRational(v[1])
        if v[0].value() == "/":
            return Fraction(parseRational(v[1]), parseRational(v[2]))
        else:
            assert False, "parsing error"
    else:
        return v

class SmtResponseParser(object):
    def __init__(self, string):
        contents = sexpdata.parse(string)
        self.result = str(contents[0].value())
        self.model = dict()
        if len(contents) > 1:
            for content in contents[1:]:
              for k, v in content:
                key = sexpdata.dumps(k)
                self.model[key.encode()] = parseRational(v)
