"""
Classes to write a smt formula using objects trees
"""
from itertools import chain
from warnings import warn
from typing import Union, Tuple, Any
from numbers import Number


class SmtFormula(object):
    """A complete SMT formula."""

    def __init__(self):
        self.lines = []
        self.options = []
        self.declarations = []
        self.directives = []
        self.declared = dict()

    def __str__(self):
        return "\n".join(chain(map(str, self.options),
                               map(str, self.declarations),
                               map(str, self.lines),
                               map(str, self.directives)))

    def declare(self, name, type, args="()"):
        # type: (str, str, str) -> None
        """add a symbol declaration"""
        if name in self.declared:
            warn("redeclaring a variable: {}".format(name))
        self.declarations.append("(declare-fun {!s} {args} {!s})".format(name, type, args=args))

    def add_declarations(self, val):
        # type: (Op) -> None
        for name, symb in val.symbolmap.iteritems():
            if name not in self.declared:
                if type(symb.type) == tuple:
                    tp = symb.type[0]
                    argtp = symb.type[1]
                else:
                    tp = symb.type
                    argtp = "()"
                self.declare(name, tp, argtp)
                self.declared[name] = symb

    def assert_(self, val):
        # type: (Op) -> None
        """add an assertion"""
        self.add_declarations(val)
        self.lines.append("(assert {!s})".format(val))

    def minimize(self, val):
        # type: (Op) -> None
        """add a optimization command"""
        self.add_declarations(val)
        self.directives.append("(minimize {!s})".format(val))

    def check_sat(self):
        """add check-sat command"""
        self.directives.append("(check-sat)")

    def get_values(self, *vs):
        # type: (*Union[str,Op]) -> None
        self.options.append("(set-option :produce-models true)")
        self.directives.append("(get-value ({}))".format(" ".join(map(str, vs))))


class Op(object):
    """A SMT operator, part of a formula, heavily use operator overloading"""

    def __init__(self, name, *params):
        # type: (str, *ValidNode) -> None
        """create an Op using the name and list of params"""
        self.name = name
        self.params = params
        self.symbolmap = dict()
        for p in params:
            if hasattr(p, "symbolmap"):
                self.symbolmap.update(p.symbolmap)

    def __str__(self):
        return "(" + str(self.name) + ' ' + ' '.join(map(str, self.params)) + ')'

    def __eq__(self, other):
        # type: (ValidNode) -> Op
        return self._associative("=", other)

    def __nonzero__(self):
        raise TypeError('SMT formula used as a boolean, will be always True as == is overloaded. '
                        'Use "is None" to check existence ')


    def __ne__(self, other):
        # type: (ValidNode) -> Op
        return ~(self == other)

    def __add__(self, other):
        # type: (ValidNode) -> Op
        return self._associative("+", other)

    def __sub__(self, other):
        # type: (ValidNode) -> Op
        other = self.fix_numbers(other)
        return Op("-", self, other)

    def __radd__(self, other):
        # type: (ValidNode) -> Op
        return self + other

    def __mul__(self, other):
        # type: (ValidNode) -> Op
        return self._associative("*", other)

    def __rmul__(self, other):
        # type: (ValidNode) -> Op
        return self._associative("*", other)

    def __and__(self, other):
        # type: (ValidNode) -> Op
        return self._associative("and", other)

    def __or__(self, other):
        # type: (ValidNode) -> Op
        return self._associative("or", other)

    def __neg__(self):
        # type: () -> Op
        return Op("-", self)

    def __invert__(self):
        return Op("not", self)

    def __ge__(self, other):
        # type: (ValidNode) -> Op
        other = self.fix_numbers(other)
        return Op(">=", self, other)

    def __le__(self, other):
        # type: (ValidNode) -> Op
        other = self.fix_numbers(other)
        return Op("<=", self, other)

    def __gt__(self, other):
        # type: (ValidNode) -> Op
        other = self.fix_numbers(other)
        return Op(">", self, other)

    def __lt__(self, other):
        # type: (ValidNode) -> Op
        other = self.fix_numbers(other)
        return Op("<", self, other)

    def _associative(self, oper, other):
        # type: (str, ValidNode) -> Op
        other = self.fix_numbers(other)
        if self.name == oper:
            return Op(oper, *(self.params + tuple([other])))
        else:
            return Op(oper, self, other)

    def fix_numbers(self, other):
        # type: (ValidNode) -> Op
        if type(other) == int or type(other) == float:
            # xxx rationals
            if other < 0:
                return Op("-", -other)
            return other
        else:
            return other


ValidNode = Union[Op, Number]


class Symbol(Op):
    """A special Op representing a variable"""

    def __init__(self, type, name, *paras):
        # type: (Union[Tuple[str], str], str, *Any) -> None
        "Initialize a symbol. type can be a tuple (ret, params) when the symbol is a function"
        super(Symbol, self).__init__(name.format(*paras))
        self.type = type
        self.symbolmap[self.name] = self

    def __str__(self):
        return str(self.name)

    def __call__(self, *args):
        # type: (*ValidNode) -> Op
        "Variables can be functions"
        ret = Op(self.name, *args)
        ret.symbolmap[self.name] = self
        return ret
