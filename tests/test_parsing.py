from unittest import  TestCase
from smtutils.parsing import parseRational, SmtResponseParser
from sexpdata import loads
from fractions import Fraction

class TestParsing(TestCase):
    def test1(self):
        a = parseRational(loads('0'))
        self.assertEqual(a, 0)
        b = parseRational(loads('(- 1)'))
        self.assertEqual(b, -1)
        c = parseRational(loads('(/ 4 8)'))
        self.assertEqual(c, Fraction(1,2))
        self.assertAlmostEqual(c, 0.5)
        d = parseRational(loads('(- (/ 3 2))'))
        self.assertEqual(d, Fraction(-3, 2))
        self.assertAlmostEqual(d, -1.5)

    def test2(self):
        r = SmtResponseParser("sat\n((a 1) (c 2) ((d e f) (- (/ 1 2))))")
        print r.model