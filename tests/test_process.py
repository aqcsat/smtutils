from unittest import  TestCase
from smtutils.process import Solver, get_msat_path
from smtutils.formula import SmtFormula, Symbol
from smtutils.parsing import SmtResponseParser

class TestProcess(TestCase):
    def test1(self):
        s = Solver(get_msat_path("optimathsat"))
        f = SmtFormula()
        a = Symbol("Int", "a")
        b = Symbol("Int", "b")
        f.assert_(a+b == 5)
        f.assert_(a < 100)
        #f.minimize(b)
        f.check_sat()
        f.get_values(a, b)
        print str(f)
        res =  s.run_formula(f)
        p = SmtResponseParser(res)
        print p.result, p.model

